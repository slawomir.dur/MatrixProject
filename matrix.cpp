#include <iostream>
#include <iomanip> //setw
#include "FinalMatrix.h"


Matrix::Matrix(int h, int w, double value)
{
    this->h = h;
    this->w = w;
    array = new double*[h];     //h mowi ile bedzie tablic w tablicy tablic czyli w macierzy,
								//tab[] to to samo co *tab   i   *tab[] to to samo co **tab,  new nie potrzebuje nazwy
    for(int i=0; i<h; ++i)
        array[i] = new double[w];

    for(int i=0; i<h; ++i)
        for(int j=0; j<w; ++j)
            array[i][j] = value;
}

Matrix::Matrix(const Matrix& mat)
{
    this->h = mat.h;
    this->w = mat.w;
    array = new double*[h];

    for(int i=0; i<h; ++i)
        array[i] = new double[w];
    
    for(int i=0; i<h; ++i)
        for(int j=0; j<w; ++j)
            array[i][j] = mat.array[i][j];
}

Matrix::Matrix(Matrix&& mat) noexcept       //tu nie moze byc const bo ta macierz mat sie zmienia
{
   this->h = mat.h;
   this->w = mat.w;
   this->array = mat.array;     //tu jest dowiazywane drugie polaczenie na dane
   mat.array = nullptr;         //tu jest usuwane pierwsze polaczenie na dane
   mat.h = 0;                      //tego juz nie ma, dlategoo jest zerowane
   mat.w = 0; 
}

Matrix::~Matrix()
{
    for(int i=0; i<h; ++i)
        delete[] array[i];
    delete[] array;
}

Matrix& Matrix::operator=(const Matrix& mat)    //operator przypisania
{
    if(this == &mat)    //adres, sprawdzamy czy wskaznik this nie wskazuje na adres mata
        return *this;  
    
    for(int i=0; i<h; ++i)
        delete[] array[i];
    delete[] array;

    h = mat.h;
    w = mat.w;
    array = new double*[h];
    for(int i=0; i<h; ++i)
        array[i] = new double[w];

    for(int i=0; i<h; ++i)
        for(int j=0; j<w; ++j)
            array[i][j] = mat.array[i][j];

    return *this;
}

Matrix& Matrix::operator=(Matrix&& mat) noexcept        //operator przeniesienia
{
    if (this == &mat)
		return *this;
	
	for(int i=0; i<h; ++i)
		delete[] array[i];
	delete[] array;

    this->h = mat.h;
    this->w =mat.w;
    this->array = mat.array;

    mat.array = nullptr;
    mat.h = 0;
    mat.w = 0;

    return *this;
}

Matrix Matrix::operator+(const Matrix& mat)
{
    Matrix result(this->h, this->w);

    for(int i=0; i<h; ++i)
        for(int j=0; j<w; ++j)
            result.array[i][j] = this->array[i][j] + mat.array[i][j];

    return result;  
}

Matrix Matrix::operator-(const Matrix& mat)
{
    Matrix result(this->h, this->w);

    for(int i=0; i<h; ++i)
        for(int j=0; j<w; ++j)
            result.array[i][j] = this->array[i][j] - mat.array[i][j];

    return result;
}

Matrix Matrix::operator*(const Matrix& mat)
{
    using namespace std;

    Matrix result(this->h, this->w, 0.0);

    for(int i=0; i<h; ++i)
        for(int j=0; j<w; ++j)
        {
            for(int k=0; k<h; ++k)
                result.array[i][j] += array[i][k] * mat.array[k][j];
        }
return result;
}

std::ostream& operator<<(std::ostream& os, const Matrix& mat)
{
    for(int i=0; i<mat.h; ++i)
    {
        for(int j=0; j<mat.w; ++j)
            os << std::setw(5) << mat.array[i][j] << " ";
        os << std::endl;
    }
    return os;
}

Matrix Matrix::Strassen(const Matrix& mat)
{
    using std::cout;
    using std::endl;

    if(this->h <= 64)
        return(*this) * mat;

    int half_h = h/2;
    int half_w = w/2;    
    
    Matrix A11(half_h, half_w);
    Matrix A12(half_h, half_w);
    Matrix A21(half_h, half_w);
    Matrix A22(half_h, half_w);

    Matrix B11(half_h, half_w);
    Matrix B12(half_h, half_w);
    Matrix B21(half_h, half_w);
    Matrix B22(half_h, half_w);

    for(int i=0; i<half_h; ++i)
        for(int j=0; j<half_w; ++j)
        {
            A11.array[i][j] = this->array[i][j];
            A12.array[i][j] = this->array[i][j+half_w];
            A21.array[i][j] = this->array[i+half_h][j];
            A22.array[i][j] = this->array[i+half_h][j+half_w];

			B11.array[i][j] = mat.array[i][j];
			B12.array[i][j] = mat.array[i][j+half_w];
			B21.array[i][j] = mat.array[i+half_h][j];
			B22.array[i][j] = mat.array[i+half_h][j+half_w];
        }

    Matrix M1((A11 + A22).Strassen(B11 + B22));
    Matrix M2((A21 + A22).Strassen(B11));
	Matrix M3(A11.Strassen(B12 - B22));
	Matrix M4(A22.Strassen(B21 - B11));
	Matrix M5((A11 + A12).Strassen(B22));
	Matrix M6((A21 - A11).Strassen(B11 + B12));
	Matrix M7((A12 - A22).Strassen(B21 + B22));
	
	Matrix C11(M1 + M4 - M5 + M7);
	Matrix C12(M3 + M5);
	Matrix C21(M2 + M4);
	Matrix C22(M1 - M2 + M3 + M6);


    Matrix result(h, w);

    for(int i=0; i<half_h; ++i)
		for (int j=0; j<half_w; ++j)
        {
            result.array[i][j] = C11.array[i][j];
            result.array[i][j+half_w] = C12.array[i][j];
            result.array[i+half_h][j] = C21.array[i][j];
            result.array[i+half_h][j+half_w] = C22.array[i][j];
        }
    return result;
}