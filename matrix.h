#pragma once
#include <iostream>
#include <memory>

class Matrix
{
    private:
        double **array;
        int h;
        int w;

    public:
        Matrix(int h, int w, double value = 0.0);
        Matrix(const Matrix& mat);
        Matrix(Matrix&& mat) noexcept; //konstruktor przeniesienia
        ~Matrix();
        Matrix& operator=(const Matrix& mat);
        Matrix& operator=(Matrix&& mat) noexcept;  //operator przeniesienia
        Matrix operator+(const Matrix& mat);
        Matrix operator-(const Matrix& mat);
        Matrix operator*(const Matrix& mat);
        double* operator[](int index) {return array[index];}    //double* - wskaznik na pierwszy element tablicy o numerze index
                                                                //to jest potrzebne, bo ta macierz jest typu Matrix
        friend std::ostream& operator<<(std::ostream& os, const Matrix& mat);
        Matrix Strassen(const Matrix& mat);
        int geth() {return h;}      //zwraca wysokosc
        int getw() {return w;}      //zwraca szerokosc
};
